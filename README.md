# Daedalus::Core
Information system which also manages all other services, clients, organizations and projects.

**Master**
[![pipeline master status](https://git.daedalus-project.io/daedalusproject/Daedalus-Core/badges/master/pipeline.svg)](https://git.daedalus-project.io/daedalusproject/Daedalus-Core/commits/master)[![gitlab master coverage report](https://git.daedalus-project.io/daedalusproject/Daedalus-Core/badges/master/coverage.svg)](https://git.daedalus-project.io/daedalusproject/Daedalus-Core/commits/master)[![Code Coverage](https://codecov.io/gh/daedalusproject/Daedalus-Core/branch/master/graph/badge.svg)](https://codecov.io/gh/daedalusproject/Daedalus-Core)

**Develop**
[![pipeline develop status](https://git.daedalus-project.io/daedalusproject/Daedalus-Core/badges/develop/pipeline.svg)](https://git.daedalus-project.io/daedalusproject/Daedalus-Core/commits/develop)[![gitlab develop coverage report](https://git.daedalus-project.io/daedalusproject/Daedalus-Core/badges/develop/coverage.svg)](https://git.daedalus-project.io/daedalusproject/Daedalus-Core/commits/develop)[![Code Coverage](https://codecov.io/gh/daedalusproject/Daedalus-Core/branch/develop/graph/badge.svg)](https://codecov.io/gh/daedalusproject/Daedalus-Core)

