use utf8;

package Daedalus::Core::Schema::CoreRealms;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use Moose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;

# Created by DBIx::Class::Schema::Loader v0.07048 @ 2019-01-26 10:35:48
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gJmxLv6AOL7VxpWVXuX3ww
__PACKAGE__->meta->make_immutable( inline_constructor => 0 );
1;
